<?php
    get_header();
?>

     <section class= "benvenuti section contenedor text-center">
        <h2 class="text-primary">
            <?php the_field('encabezado'); ?>
        </h2>
        <p><?php the_field('testo_benvenuto'); ?></p>
     </section>

     <section class="areas">

        <div class="area" >
            <?php $area1 =  get_field('area_1');
                $imagen = $area1['imagen']['sizes']['medium_large'];
                $texto = $area1['texto'];  
                ?>         
            <img src="<?php echo  esc_attr($imagen);?>" alt="Imagen <?php echo esc_attr($texto)?>">
            <p><?php echo esc_html($texto);?></p>
        </div>

        <div class="area" >
            <?php $area2 =  get_field('area_2');
                $imagen = $area2['imagen']['sizes']['medium_large'];
                $texto = $area2['texto'];  
                ?>         
            <img src="<?php echo  esc_attr($imagen);?>" alt="Imagen <?php echo esc_attr($texto)?>">
            <p><?php echo esc_html($texto);?></p>
        </div>

        <div class="area" >
            <?php $area3 =  get_field('area_3');
                $imagen = $area3['imagen']['sizes']['medium_large'];
                $texto = $area3['texto'];  
                ?>         
            <img src="<?php echo  esc_attr($imagen);?>" alt="Imagen <?php echo esc_attr($texto)?>">
            <p><?php echo esc_html($texto);?></p>
        </div>


        <div class="area" >
            <?php $area4 =  get_field('area_4');
                $imagen = $area4['imagen']['sizes']['medium_large'];
                $texto = $area4['texto'];  
                ?>         
            <img src="<?php echo  esc_attr($imagen);?>" alt="Imagen <?php echo esc_attr($texto)?>">
            <p><?php echo esc_html($texto);?></p>
        </div>


        


     </section>

    <main class="contenedor seccion">

        <h2 class="text-center text-primary">NUESTRAS CLASES</h2>

        <?php gymfitness_lista_clases(4); ?>
        <div class="contenedor-boton">
            <a href="<?php echo esc_url(get_permalink(get_page_by_title('Lezioni')));?>" class="boton boton-primario">
                VER TODAS LAS CLASES
        </a>
        </div>
        
    </main>

    <section class="contenedor seccion">
        <h2 class="text-center text-primary">
            Nostri instruttori
        </h2>
        <p class="text-center">
        Istruttori professionali che ti aiutano a raggiungere i tuoi obiettivi
        </p>
        <?php gymfitness_instruttori();?>
    </section>

    <section class="testimoniales">

        <h2 class="text-center texto-blanco ">
            Cosa dicono i nostri clienti
        </h2>
        <div class="contenedor-testimoniales swiper">
            <?php gymfitness_testimoniales(); ?>
        </div>

    </section>

    <section class="contenedor seccion">
        <h2 class="text-center text-primary">Nostro blog</h2>
        <p class="text-center">Impara tips dei nostri insegnanti</p>

        <ul class="listado-grid">
            <?php 
               $args = array(
                    'post_type' =>  'post',
                    'posts_per_page' => 4
               );
               $blog = new WP_query($args);
               while($blog->have_posts()) {
                $blog->the_post(); 

                get_template_part('template-parts/blog');
               }
               wp_reset_postdata();
            ?>   
        </ul>
    </section>
    
<?php
    get_footer();
?>




