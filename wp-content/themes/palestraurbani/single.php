<?php
    get_header();
?>

    <main class="contenedor seccion">
        <?php
    
            get_template_part('template-parts/post');
        ?>

        <div class="commenti">
            <?php comment_form() ?>

            <h3 class="text-center text-primary">Commenti</h3>
            <ul class="lista-commenti">
                <?php
                  $commenti = get_comments(array(
                    'post_id' => $post->ID,
                    'status' => 'approve'
                  ));
                  wp_list_comments(array(
                    'per_page' => 10,
                    'reverse_top_level' => false,

                  ), $commenti) 
                ?>
            </ul>

        </div>

    </main>
    
<?php
    get_footer();
?>




