<?php

function gymfitness_lista_clases($cantidad = -1) {
    ?>
          <ul class="listado-grid">
            <?php
               
                $args = array(
                    'post_type' => 'gymfitness_clases',
                    'posts_per_page' => $cantidad
                );
                $clases = new WP_Query($args);

                while($clases->have_posts()) {
                    $clases->the_post();
                 ?> 
                    <li class="card">

                        <?php the_post_thumbnail();?>
                        <div class="contenido">
                            <a href="<?php the_permalink()?>">
                                <h3><?php the_title(); ?></h3>
                            </a>
                            <?php
                                $ora_inizio = get_field('ora_inizio');
                                $ora_fine = get_field('ora_fine');
                            
                            ?>
                            
                            <p><?php the_field('giorni_lezioni'); ?>
                                <?php echo $ora_inizio . "-" . $ora_fine ?>
                            </p>
                        </div>          

                    </li>
            <?php
                }
                wp_reset_postdata();
            ?>
        </ul>  

    <?php
    
}

function gymfitness_instruttori() {
    ?>
    <ul class="listado-grid instruttori">
            <?php   
                $args = array(
                    'post_type' => 'instructores',     
                );
                $instruttori = new WP_Query($args);

                while($instruttori->have_posts()) {
                    $instruttori->the_post();
                 ?> 
                    <li class="instruttore">
                        <?php the_post_thumbnail('large');?>
                        <div class="contenido text-center">
                            <h3><?php the_title(); ?></h3>
                            <?php the_content();?>
                        
                            <div class="especialidad">
                                <?php
                                    $esp = get_field('especialidad');

                                    foreach($esp as $es) { ?>

                                        <span class="tag">
                                            <?php echo $es; ?>
                                        </span>
                                      <?php  
                                    }
                                    ?>
                            </div>
                        </div>
                    </li>
            <?php
                }
                wp_reset_postdata();
            ?>
        </ul>  
    <?php

}

function gymfitness_testimoniales() {
    ?>

<ul class="listado-testimoniales swiper-wrapper">
    <?php   
        $args = array(
            'post_type' => 'testimoniales',     
        );
        $testimoniales = new WP_Query($args);

        while($testimoniales->have_posts()) {
            $testimoniales->the_post();
    ?> 
        <li class="testimonial text-center swiper-slide">
            <blockquote>
                <?php the_content();?>
            </blockquote>

            <footer class="testimonial-footer">
                <?php the_post_thumbnail('thumbnail'); ?>
                <p>
                    <?php the_title(); ?>
                </p>

            </footer>

        </li>
        <?php
            }
            wp_reset_postdata();
        ?>
</ul>  

  <?php
}

