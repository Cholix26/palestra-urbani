<?php

while( have_posts() ): the_post();

                the_title('<h1 class="text-center text-primary">','</h1>');

                if(has_post_thumbnail()){

                    the_post_thumbnail('full', array('class' => 'imagen-destacada'));

                }
                
                    $ora_inizio = get_field('ora_inizio');
                    $ora_fine = get_field('ora_fine');
                
                ?>
                <p class="informacion-lezioni">
                    <?php the_field('giorni_lezioni'); ?>
                        <?php echo $ora_inizio . "-" . $ora_fine ?>
                </p>
                <?php
                
                the_content();

endwhile;
?>